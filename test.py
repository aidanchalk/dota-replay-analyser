import subprocess
import requests
import json
import modules.heroes as heroes
import modules.teams as teams
import modules.items as items
import math as math
import array

def slot_to_hero_id_map(interval_zero):
    slots_to_hero_id = { 0:None, 1: None, 2: None, 3:None, 4: None, 5:None, 6:None, 7:None, 8:None, 9: None}
    for line in interval_zero:
        slots_to_hero_id[line['slot']] = line['hero_id']
    return slots_to_hero_id

def print_slot_to_hero_name(slots_to_hero_id):
    for i in range(0,10):
        print("Slot {} hero {}".format(i, heroes.get_hero_name(slots_to_hero_id[i])))

def get_interval_zero(data):
    interval_zero = []
    for line in data:
        if line['type'] == 'interval':
            if line['time'] == 0:
                interval_zero.append(line)
    return interval_zero

def team_bounty_runes(bounty_info):
    max_time = 0
    for line in bounty_info:
        if line['time'] > max_time:
            max_time = line['time']
    radiant_count = {}
    dire_count = {}
    bounty_runes = { teams.DIRE : dire_count, teams.RADIANT : radiant_count  }
    for line in bounty_info:
        time = int(math.floor(line['time'] / 300.0))*300
        team = teams.SLOTS[line['player1']]
        bounty_runes[team][time] = bounty_runes[team].get(time, 0) + 1
    for i in range(0, max_time+1, 300):
        print("Minute {} bounty runes: Radiant [{}] Dire [{}]".format(i//60, radiant_count.get(i, 0), dire_count.get(i,0)))

def get_bounty_pickups(bounty_info, slot_to_hero_id):
    for line in bounty_info:
        print("Bounty rune at {} picked up by hero {}".format(line['time'], heroes.get_hero_name(slot_to_hero_id[line['player1']])))

def get_bounty_runes(data):
    bounty_info = []
    for line in data:
        if line['type'] == 'CHAT_MESSAGE_RUNE_PICKUP':
            if line['value'] == 5:
                bounty_info.append(line)
    return bounty_info

def get_draft_info(data):
    draft_info = []
    for line in data:
        if line['type'] == 'draft_timings':
          draft_info.append(line)
    return draft_info

def print_draft(first_pick, rad_ban, dire_ban, rad_pick, dire_pick):
    team_names = { 2: 'DIRE', 3: 'RADIANT'}
    counter_bans = { 2: 0, 3:0}
    counter_picks = {2:0, 3:0}
    team_bans = { 2:dire_ban, 3:rad_ban}
    team_picks = {2:dire_pick, 3:rad_pick}
    swap = {2:3, 3:2}
    current = first_pick
    for i in range(0,6):
        print('{} BAN: {}'.format(team_names[current], team_bans[current][counter_bans[current]]))
        counter_bans[current]+=1
        current = swap[current]
    print('{} PICK: {}'.format(team_names[current], team_picks[current][counter_picks[current]]))
    counter_picks[current]+=1
    current = swap[current]
    print('{} PICK: {}'.format(team_names[current], team_picks[current][counter_picks[current]]))
    counter_picks[current]+=1
    print('{} PICK: {}'.format(team_names[current], team_picks[current][counter_picks[current]]))
    counter_picks[current]+=1
    current = swap[current]
    print('{} PICK: {}'.format(team_names[current], team_picks[current][counter_picks[current]]))
    counter_picks[current]+=1
    for i in range(0,4):
        print('{} BAN: {}'.format(team_names[current], team_bans[current][counter_bans[current]]))
        counter_bans[current]+=1
        current = swap[current]
    current = swap[current]
    for i in range(0,4):
        print('{} PICK: {}'.format(team_names[current], team_picks[current][counter_picks[current]]))
        counter_picks[current]+=1
        current = swap[current]
    print('{} BAN: {}'.format(team_names[current], team_bans[current][counter_bans[current]]))
    counter_bans[current]+=1
    current = swap[current]
    print('{} BAN: {}'.format(team_names[current], team_bans[current][counter_bans[current]]))
    counter_bans[current]+=1
    print('{} PICK: {}'.format(team_names[current], team_picks[current][counter_picks[current]]))
    counter_picks[current]+=1
    current = swap[current]
    print('{} PICK: {}'.format(team_names[current], team_picks[current][counter_picks[current]]))
    counter_picks[current]+=1
    current = swap[current]


def draft_analysis(draft_info):
    rad_ban = []
    dire_ban = []
    rad_pick = []
    dire_pick = []
    rad_hero_ids = []
    dire_hero_ids = []
    team_bans = { 2:dire_ban, 3:rad_ban}
    team_picks = {2:dire_pick, 3:rad_pick}
    team_ids = {2:dire_hero_ids, 3:rad_hero_ids}
    swap = {2:3, 3:2}

    #Find out who first pick is
    first_pick = draft_info[0]['draft_active_team']
    current = first_pick
    #Get the initial bans
    for i in range(0,6):
        team_bans[current].append(heroes.get_hero_name(draft_info[i]['hero_id']))
        current = swap[current]
    #Get stage one picks
    team_picks[current].append(heroes.get_hero_name(draft_info[6]['hero_id']))
    team_ids[current].append(draft_info[6]['hero_id'])
    current = swap[current]
    team_picks[current].append(heroes.get_hero_name(draft_info[7]['hero_id']))
    team_ids[current].append(draft_info[7]['hero_id'])
    team_picks[current].append(heroes.get_hero_name(draft_info[8]['hero_id']))
    team_ids[current].append(draft_info[8]['hero_id'])
    current = swap[current]
    team_picks[current].append(heroes.get_hero_name(draft_info[9]['hero_id']))
    team_ids[current].append(draft_info[9]['hero_id'])
    #Second stage bans
    for i in range(10,14):
        team_bans[current].append(heroes.get_hero_name(draft_info[i]['hero_id']))
        current = swap[current]
    #2nd pick keeps first pick in 2nd phase picks
    current = swap[current]
    for i in range(14,18):
        team_picks[current].append(heroes.get_hero_name(draft_info[i]['hero_id']))
        team_ids[current].append(draft_info[i]['hero_id'])
        current = swap[current]
    #Final bans
    team_bans[current].append(heroes.get_hero_name(draft_info[18]['hero_id']))
    current = swap[current]
    team_bans[current].append(heroes.get_hero_name(draft_info[19]['hero_id']))
    #Final picks
    team_picks[current].append(heroes.get_hero_name(draft_info[20]['hero_id']))
    team_ids[current].append(draft_info[20]['hero_id'])
    current = swap[current]
    team_picks[current].append(heroes.get_hero_name(draft_info[21]['hero_id']))
    team_ids[current].append(draft_info[21]['hero_id'])

    print_draft(first_pick, rad_ban, dire_ban, rad_pick, dire_pick)
    return team_ids

def main():
    subprocess.call('curl localhost:5600 -s --data-binary "@./test.dem" > file.json', shell=True)
#    f = open('test.dem', 'rb')
#    r = requests.post('http://localhost:5600', data = f)
#    f.close()
    data = []
#    json_lines = r.text.split('\n')
    with open('file.json', 'r') as json_lines:
        for line in json_lines:
            data.append( json.loads(line))

    rune_data = []
    epilogue_key = None
    death_data = []
    roshan_data = []
    interval_data = {0:[], 1:[], 2:[], 3:[], 4:[], 5:[], 6:[], 7:[], 8:[], 9:[]}
    ten_min_data = {0:None, 1:None, 2:None, 3:None, 4:None, 5:None, 6:None, 7:None, 8:None, 9:None}
    twenty_min_data = {0:None, 1:None, 2:None, 3:None, 4:None, 5:None, 6:None, 7:None, 8:None, 9:None}
    combat_damage_data = []
    gold_data = []
    xp_data = []
    multikill_data = []
    firstblood_data = []
    buyback_data = []
    killstreak_data = []
    draft_info = []
    damage_data = []
    purchase_data = []
    aegis_data = []
    heal_data = []
    interval_zero = []
    item_data = []
 
    for line in data:
        if line['type'] == 'CHAT_MESSAGE_RUNE_PICKUP':
            rune_data.append(line)
        elif line['type'] == 'interval':
            interval_data[line['slot']].append(line)
            if line['time'] == 0:
                interval_zero.append(line)
            elif line['time'] == 600:
                ten_min_data[line['slot']] = line
            elif line['time'] == 1200:
                twenty_min_data[line['slot']] = line
        elif line['type'] == 'draft_timings':
            draft_info.append(line)
        elif line['type'] == 'epilogue':
            print('hello')
            epilogue_key = json.loads(line['key'])
        elif line['type'] == 'DOTA_COMBATLOG_DEATH':
            death_data.append(line)
        elif line['type'] == 'CHAT_MESSAGE_ROSHAN_KILL':
            roshan_data.append(line)
        elif line['type'] == 'CHAT_MESSAGE_COMBATLOG_DAMAGE':
            combat_damage_data.append(line)
        elif line['type'] == 'DOTA_COMBATLOG_GOLD':
            gold_data.append(line)
        elif line['type'] == 'DOTA_COMBATLOG_XP':
            xp_data.append(line)
        elif line['type'] == 'DOTA_COMBATLOG_MULTIKILL':
            multikill_data.append(line)
        elif line['type'] == 'CHAT_MESSAGE_FIRSTBLOOD':
            firstblood_data.append(line)
        elif line['type'] == 'DOTA_COMBATLOG_BUYBACK':
            firstblood_data.append(line)
        elif line['type'] == 'DOTA_COMBATLOG_KILLSTREAK':
            killstreak_data.append(line)
        elif line['type'] == 'DOTA_COMBATLOG_DAMAGE':
            damage_data.append(line)
        elif line['type'] == 'DOTA_COMBATLOG_PURCHASE':
            purchase_data.append(line)
        elif line['type'] == 'CHAT_MESSAGE_AEGIS':
            aegis_data.append(line)
        elif line['type'] == 'DOTA_COMBATLOG_HEAL':
            heal_data.append(line)
        elif line['type'] == 'DOTA_COMBATLOG_PURCHASE':
            item_data.append(line)






#    types = set()
#    for line in data:
#        types.add(line['type'])

#    draft_info = get_draft_info(data)
    #for line in draft_info:
    #    print(line)
    #    print(heroes.get_hero_name(line['hero_id']))
    team_ids = draft_analysis(draft_info)
    
#    interval_zero = get_interval_zero(data)
    slot_to_hero_id = slot_to_hero_id_map(interval_zero)
    bounty_info = get_bounty_runes(data)
    #print(len(bounty_info))
    #print(bounty_info)
    print_slot_to_hero_name(slot_to_hero_id)
    get_bounty_pickups(bounty_info, slot_to_hero_id)
    team_bounty_runes(bounty_info)

    slot_to_player_name = { 0:None, 1: None, 2: None, 3:None, 4: None, 5:None, 6:None, 7:None, 8:None, 9: None}
 #   for line in data:
 #       if(line['type'] == 'epilogue'):
 #           epilogue_key = json.loads(line['key'])
    print(epilogue_key)
    for playerInfo in epilogue_key['gameInfo_']['dota_']['playerInfo_']:
        print('{} is playing {}'.format(bytes(playerInfo['playerName_']['bytes']).decode('utf-8'), heroes.heroes[bytes(playerInfo['heroName_']['bytes']).decode('utf-8')]['localized_name']))
        for slots in slot_to_hero_id:
            if slot_to_hero_id[slots] == heroes.heroes[bytes(playerInfo['heroName_']['bytes']).decode('utf-8')]['id']:
                slot_to_player_name[slots] = bytes(playerInfo['playerName_']['bytes']).decode('utf-8')
    print(slot_to_hero_id)
    print(slot_to_player_name)

    print(roshan_data)
#    print(interval_data[3][-2])

    print('{} Net worth at {}:{} is {}'.format(heroes.get_hero_name(slot_to_hero_id[interval_data[0][3000]['slot']]), interval_data[0][3000]['time']//60, interval_data[0][3000]['time']-interval_data[0][3000]['time']//60 *60 ,  interval_data[0][3000]['networth']))
    print('{} Net worth at {}:{} is {}'.format(heroes.get_hero_name(slot_to_hero_id[interval_data[0][-2]['slot']]), interval_data[0][-2]['time']//60, interval_data[0][-2]['time']-interval_data[0][-2]['time']//60 *60 ,  interval_data[0][-2]['networth']))

    gold_gained = 0
    gold_lost = 0
    gold = 0
    for line in gold_data:
        if line['targetname'] == 'npc_dota_hero_sven':
            if line['value'] < 0:
                gold_lost = gold_lost - line['value']
               #print(line)
            else:
                gold_gained = gold_gained + line['value']
            gold = gold + line['value']
    print(gold_lost)
    print(gold_gained)
    print(gold)
    consumable_gold = 0
    for line in purchase_data:
        if line['targetname'] == 'npc_dota_hero_sven':
            item = items.get_consumable(line['valuename'])
            if item != None:
                consumable_gold = consumable_gold + item['cost']
#print(items[line['value']])
    print(consumable_gold)
    print(ten_min_data[0])
    print(twenty_min_data[0])
 #   print(types)

    print('name, hero, net worth, kills, deaths, assists, last hits, denies, teamfight participation, gpm, xpm, lh@10,deny@10,nw@10,k@10,d@10,a@10, lh@20, deny@20, nw@20,k@20,d@20,a@20')
    for slot in slot_to_hero_id:
        networth = interval_data[slot][-1]['networth']
        kills = interval_data[slot][-1]['kills']
        deaths = interval_data[slot][-1]['deaths']
        assists = interval_data[slot][-1]['assists']
        lh = interval_data[slot][-1]['lh']
        denies = interval_data[slot][-1]['denies']
        teamfight_part = interval_data[slot][-1]['teamfight_participation']
        gpm = interval_data[slot][-1]['gold'] / (interval_data[slot][-1]['time']/60)
        xpm = interval_data[slot][-1]['xp'] / (interval_data[slot][-1]['time']/60)
        name = slot_to_player_name[slot]
        hero = heroes.get_hero_name(slot_to_hero_id[slot])
        cs_at_10 = ten_min_data[slot]['lh']
        cs_at_20 = twenty_min_data[slot]['lh']
        nw_at_10 = ten_min_data[slot]['networth']
        nw_at_20 = twenty_min_data[slot]['networth']
        deny_at_10 = ten_min_data[slot]['denies']
        deny_at_20 = twenty_min_data[slot]['denies']
        kill_at_10 = ten_min_data[slot]['kills']
        kill_at_20 = twenty_min_data[slot]['kills']
        death_at_10 = ten_min_data[slot]['deaths']
        death_at_20 = twenty_min_data[slot]['deaths']
        assist_at_10 = ten_min_data[slot]['assists']
        assist_at_20 = twenty_min_data[slot]['assists']
        print('{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}'.format(name, hero, networth,kills,deaths,assists,lh,denies,teamfight_part,gpm,xpm,
            cs_at_10,deny_at_10,nw_at_10,kill_at_10,death_at_10,assist_at_10,cs_at_20,deny_at_20,nw_at_20,kill_at_20,death_at_20,assist_at_20))


if __name__ == "__main__": 
    main()
